<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    protected $fillable=[
        'problema',
        'descripcion',
        'estado',

    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
