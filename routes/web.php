<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function () {
    return view('home');
});

Route::get('/', function () {
    return view('auth.login');
});

//Administrador
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/administrador', function () {
     return view('Administrador.Dashboard');
     
 });
 
 Route::resource('administrador','AdminController');

});

//Tickets
Route::get('/ticket/create','TicketController@create');
Route::post('/ticket','TicketController@store');
Route::get('/tickets','TicketController@index');



Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');