@extends('layouts.app')

@section('content')

@extends('Menus.MenuUsuario')

<div class="container mt-5">
<form action="/ticket" method="POST" >
@csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Problema:</label>
      <input type="text" class="form-control" name="problema" placeholder="Menciona tu problema"> 
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1" >Descripcion del problema:</label>
        <textarea class="form-control" name="descripcion" style="resize: none; overflow: auto;"></textarea>
      </div>
    
    <button type="submit" class="btn btn-primary">Crear Ticket</button>
  </form>
</div>
@endsection