@extends('layouts.app')

@section('content')

@extends('Menus.MenuUsuario')
<div class=" container table-responsive mt-5 ">
<form action="/ticket/create" method="GET" enctype="multipart/form-data">
        @csrf
    <button type="submit" class="btn btn-primary btn-lg" >
        Generar Ticket
    </button>
    </form>

   
      <table class="table table-striped table-bordered table-hover mt-1">
   
    <tr>
<th>#Nodo</th>
<th>Problema</th>
<th>Descripcion</th>
<th>Estado</th>
<th>Fecha de Creacion</th>
      </tr>
@foreach ($tickets as $key=>$data)
<tr>
<td>{{++$key}}</td>
<td>{{$data->problema}}</td>
<td>{{$data->descripcion}}</td>
<td>{{$data->estado}}</td>
<td>{{$data->created_at}}</td>
</tr>
@endforeach
        </table>
    </div>
@endsection
