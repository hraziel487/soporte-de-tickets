<nav class="navbar navbar-expand-lg navbar-light bg-light">
    @if (Auth::check())
   {{ auth()->user()->name}}
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
     </button>
     <div class="collapse navbar-collapse" id="navbarNav">
       <ul class="nav navbar-nav ml-auto">
        
         
         <li class="nav-item m-2">
             {{ auth()->user()->email}}
         </li>
         <li class="nav-item m-2">
             <a href="{{ url('/logout') }}"> logout </a>
         </li>
         @endif
       </ul>
     </div>
   </nav>