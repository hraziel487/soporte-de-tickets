@extends('layouts.app')

@section('content')

@extends('Menus.MenuAdmin')

<br>

    <div class="container table-responsive">
        <table class="table table-striped table-bordered table-hover">
  
    <tr>
<th>#Nodo</th>
<th>Problema</th>
<th>Descripcion</th>
<th>Estado</th>
<th>Fecha de Creacion</th>
<th>Resolver</th>
      </tr>
@foreach ($tickets as $key=>$data)
<tr>
<td>{{++$key}}</td>

<td>{{$data->problema}}</td>
<td>{{$data->descripcion}}</td>
<td>{{$data->estado}}</td>
<td>{{$data->created_at}}</td>
<td><a class="btn btn-success" style="background: #69bb85!important; border-color:#69bb85!important;" href="{{action('AdminController@edit', $data->id)}}">Resolver </a></td>
</tr>
@endforeach
        </table>
    </div>
        @endsection
