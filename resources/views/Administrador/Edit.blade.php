@extends('layouts.app')

@section('content')

@extends('Menus.MenuAdmin')

<div class="container mt-5">
<form method="post" action="{{ route('administrador.update', $data->id) }}">
    @method('PATCH') 
    @csrf
    <div class="form-group">

        <label for="first_name">Problema:</label>
        <input type="text" class="form-control" disabled value={{ $data->problema}} />
    </div>

    <div class="form-group">
        <label for="last_name">Descripcion:</label>
        <input type="text" class="form-control" disabled value={{ $data->descripcion }} />
    </div>

    <div class="form-group">
        <label for="last_name">Estado:</label>
        <select id="inputState" class="form-control" name="estado">
            <option value="Via Remota">Via Remota</option>
            <option value="Presencial">Presencial</option>
          </select>
    </div>
   
    <button type="submit" class="btn btn-primary" style="background: #69bb85!important; border-color:#69bb85!important;">Actualizar</button>
</form>
</div>
@endsection